set nocompatible              " be iMproved, required
filetype off                  " required

""""""""""""""""""""""""""""""
" => Vundle Settings
""""""""""""""""""""""""""""""
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
" call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'scrooloose/nerdTree'

Plugin 'octol/vim-cpp-enhanced-highlight'

"
" Visual related plugins
Plugin 'bling/vim-airline'
set laststatus=2
set ttimeoutlen=50
let g:airline_powerline_fonts = 1
Plugin 'sickill/vim-monokai'

"
" Git related plugins
Plugin 'airblade/vim-gitgutter'
let g:gitgutter_eager = 1
let g:gitgutter_realtime = 1
let g:gitgutter_updatetime = 1000
Plugin 'tpope/vim-fugitive'

"
" Latex related plugins
Plugin 'lervag/vimtex'
" Requires vim build with +clientserver but most builds are not
let g:vimtex_latexmk_callback = 0

"
" Editing related plugins
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-commentary'

"
" Fuzzy search plugin
Plugin 'ctrlpvim/ctrlp.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
"Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Avoid a name conflict with L9
"Plugin 'user/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

""""""""""""""""""""""""""""""
" => Standard VIM
""""""""""""""""""""""""""""""
" Set to auto read when a file is changed from the outside
" set autoread

" Look
syntax on

let mapleader=","

colorscheme monokai
set number
set relativenumber
set ruler
set background=dark

" Case handling
set ignorecase
set smartcase
" backspace over ...
set backspace=eol,start,indent
" keys that allow switching to previous/next line
set whichwrap+=<,>,h,l

" highlight search
set hlsearch
" search while typing pattern
set incsearch
" special characters in search patterns - default in vim
set magic
" briefly jump to matching bracket
set showmatch
set mat=2

" replace tab with spaces
" set expandtab
" set smarttab
" Number of spaces to use for each step of (auto)indent.
set shiftwidth=8
" Number of spaces that a <Tab> in the file counts for.
set tabstop=8
" better linebreaks
set lbr
" break lines at edge of window
set wrap
" copy indent from current line when starting a new line
set ai
" smart indent when starting a new line
set si
" Mouse support
set mouse=a
" Copy to Clipboard
:set clipboard=unnamed

" Search down into subfolders
" Provides tab-completion for all file-related tasks
set path+=**

" Display all matching files when we tab complete
set wildmenu

" Higlight long lines but only if they are actually to long
highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%81v', 100)

""""""""""""""""""""""""""""""
" => Normal mode related
""""""""""""""""""""""""""""""
" Spell checking
nnoremap <leader>s :set nospell<CR>
nnoremap <leader>sen :setlocal spell spelllang=en_gb<CR>
nnoremap <leader>sde :setlocal spell spelllang=de_20<CR>

hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red
hi clear SpellCap
hi SpellCap cterm=underline ctermfg=blue
hi clear SpellRare
hi SpellRare cterm=underline ctermfg=blue
hi clear SpellLocal
hi SpellLocal cterm=underline ctermfg=blue

hi clear Search
hi Search cterm=underline ctermfg=magenta
""""""""""""""""""""""""""""""
" => Visual mode related
""""""""""""""""""""""""""""""
" Visual mode pressing * or # searches for the current selection
vnoremap <silent> * :call VisualSelection('f')<CR>
vnoremap <silent> # :call VisualSelection('b')<CR>

""""""""""""""""""""""""""""""
" => Autocommands
""""""""""""""""""""""""""""""
" Remove trailing whitespaces
autocmd BufWritePre * :%s/\s\+$//e
